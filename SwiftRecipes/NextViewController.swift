//
//  NextViewController.swift
//  SwiftRecipe
//
//  Created by Francisco Morales on 9/29/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit

class NextViewController: UIViewController {
    var info: Dictionary<String, String>?
    @IBOutlet var titleLbl: UILabel!
    
    @IBOutlet var ImageView: UIImageView!
    @IBOutlet var bioInfoTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLbl.text = info?["name"]
        
        bioInfoTextView.text = info?["ingredientsteps"]
        
        let url = URL(string: (info?["image"])!)
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!) 
            DispatchQueue.main.async {
                self.ImageView.image = UIImage(data: data!)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
