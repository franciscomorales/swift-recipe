//
//  ViewController.swift
//  SwiftRecipe
//
//  Created by Francisco Morales on 9/29/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var myArray: NSArray?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (myArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let obj = myArray?.object(at: indexPath.row) as! Dictionary<String, String>
        cell.textLabel?.text = obj["name"]
//        cell.textLabel?.text = "Test"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let obj = myArray?.object(at: indexPath.row) as! Dictionary<String, String>
        performSegue(withIdentifier: "move", sender: obj)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let path = Bundle.main.path(forResource: "recipes", ofType: "plist") {
            myArray = NSArray(contentsOfFile: path)
        }
        print(myArray as Any)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextVc = segue.destination as! NextViewController
        nextVc.info = sender as? Dictionary<String, String>
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

